import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import PageHome from './src/components/PageHome';
import PageCloset from './src/components/PageCloset';
import PageFashionRegistration from './src/components/PageFashionRegistration';
import PageProposal from './src/components/PageProposal';
import PageSetting from './src/components/PageSetting';
import PageColorChoiceJacket from './src/components/PageColorChoiceJacket';
import PageColorChoiceShirt from './src/components/PageColorChoiceShirt';
import PageColorChoiceNinepants from './src/components/PageColorChoiceNinepants';
import PageColorChoicePencilskirt from './src/components/PageColorChoicePencilskirt';
import PageColorChoiceCutAndSewn from './src/components/PageColorChoiceCutAndSewn';
import PageColorChoiceCardigan from './src/components/PageColorChoiceCardigan';
import PageColorChoiceMidiSkirt from './src/components/PageColorChoiceMidiSkirt';
import PageColorChoiceKnit from './src/components/PageColorChoiceKnit';
import PageColorChoiceGaucho from './src/components/PageColorChoiceGaucho';


export default class App extends Component {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="PageHome" component={PageHome} hideNavBar={true} initial={true}/>
          <Scene key="PageCloset" component={PageCloset} hideNavBar={true} />
          <Scene key="PageFashionRegistration" component={PageFashionRegistration} hideNavBar={true}/>
          <Scene key="PageColorChoiceJacket" component={PageColorChoiceJacket} hideNavBar={true} />
          <Scene key="PageColorChoiceShirt" component={PageColorChoiceShirt} hideNavBar={true} />
          <Scene key="PageColorChoiceNinepants" component={PageColorChoiceNinepants} hideNavBar={true} />
          <Scene key="PageColorChoicePencilskirt" component={PageColorChoicePencilskirt} hideNavBar={true} />
          <Scene key="PageColorChoiceCutAndSewn" component={PageColorChoiceCutAndSewn} hideNavBar={true} />
          <Scene key="PageColorChoiceCardigan" component={PageColorChoiceCardigan} hideNavBar={true} />
          <Scene key="PageColorChoiceKnit" component={PageColorChoiceKnit} hideNavBar={true} />
          <Scene key="PageColorChoiceMidiSkirt" component={PageColorChoiceMidiSkirt} hideNavBar={true} />
          <Scene key="PageColorChoiceGaucho" component={PageColorChoiceGaucho} hideNavBar={true} />
          <Scene key="PageProposal" component={PageProposal} hideNavBar={true} />
          <Scene key="PageSetting" component={PageSetting} hideNavBar={true} />
        </Scene>
      </Router>
    )
  }
}
