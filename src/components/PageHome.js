import React, { Component } from 'react';
import { View, Image, StyleSheet,AppRegistry,ListView, } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Icon,
         Title, Body, Left, Right, Tab, Tabs, Card, CardItem, Thumbnail,
         TabHeading, Text} from 'native-base';
import { Actions } from 'react-native-router-flux';
import styles from '../styles';

import firebaseApp from '../library/firebaseApp'


const Tab1 = () => (<View />);
const Tab2 = () => (<View />);
const Tab3 = () => (<View />);
const Tab4 = () => (<View />);
const Tab5 = () => (<View />);

export default class PageHome extends Component {

  constructor(props) {
    super(props);
    this.state = {
      cordeDataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
    this.cordeItemsRef = this.getRef().child('トップス');
  }
  getRef() {
    return firebaseApp.database().ref();
  }

  listenForCordeItems(cordeItemsRef) {
    cordeItemsRef.on('value', (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
      items.push({
          title: child.val().fashionId ,
          _key: child.key
        });
      });
      this.setState({
        cordeDataSource: this.state.cordeDataSource.cloneWithRows(items)
      });
    });
  }
  componentDidMount() {
    this.listenForCordeItems(this.cordeItemsRef);
  }


  render() {
    const data = this.props.data;
    return (
      <Container>
      <Header style={styles.headerBackStyle}>
        <Body>
          <Title>SideProject</Title>
        </Body>
      </Header>
        <Tabs>
          <Tab heading="MON">
            <Tab1/>
              <Content>
                <Card>
                  <CardItem cardBody>
                    <Image source={require('../image/sample/fashionImage1.jpg')} style={{height: 450, width: null, flex: 1}}/>
                  </CardItem>
                  <CardItem style={{height: 30, width: null, flex: 1}}>
                    <Left>
                      <Button transparent>
                        <Icon active name="thumbs-up" />
                      <Text>12 Likes</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button transparent>
                      <Icon active name="chatbubbles" />
                      <Text>4 Comments</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem cardBody>
                    <Image source={require('../image/sample/fashionImage2.jpg')} style={{height: 450, width: null, flex: 1}}/>
                  </CardItem>
                  <CardItem style={{height: 30, width: null, flex: 1}}>
                    <Left>
                      <Button transparent>
                        <Icon active name="thumbs-up" />
                      <Text>12 Likes</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button transparent>
                      <Icon active name="chatbubbles" />
                      <Text>4 Comments</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
                <Card>
                  <CardItem cardBody>
                    <Image source={require('../image/sample/fashionImage3.jpg')} style={{height: 450, width: null, flex: 1}}/>
                  </CardItem>
                  <CardItem style={{height: 30, width: null, flex: 1}}>
                    <Left>
                      <Button transparent>
                        <Icon active name="thumbs-up" />
                      <Text>12 Likes</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button transparent>
                      <Icon active name="chatbubbles" />
                      <Text>4 Comments</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
              </Content>
            </Tab>

          <Tab heading="TUE">
            <Tab2 />
            <Content>
              <Card>
                <CardItem cardBody>
                  <Image source={require('../image/sample/fashionImage4.jpg')} style={{height: 450, width: null, flex: 1}}/>
                </CardItem>
                <CardItem style={{height: 30, width: null, flex: 1}}>
                  <Left>
                    <Button transparent>
                      <Icon active name="thumbs-up" />
                    <Text>12 Likes</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button transparent>
                    <Icon active name="chatbubbles" />
                    <Text>4 Comments</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
              <Card>
                <CardItem cardBody>
                  <Image source={require('../image/sample/fashionImage5.jpg')} style={{height: 450, width: null, flex: 1}}/>
                </CardItem>
                <CardItem style={{height: 30, width: null, flex: 1}}>
                  <Left>
                    <Button transparent>
                      <Icon active name="thumbs-up" />
                    <Text>12 Likes</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button transparent>
                    <Icon active name="chatbubbles" />
                    <Text>4 Comments</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
              <Card>
                <CardItem cardBody>
                  <Image source={require('../image/sample/fashionImage6.jpg')} style={{height: 450, width: null, flex: 1}}/>
                </CardItem>
                <CardItem style={{height: 30, width: null, flex: 1}}>
                  <Left>
                    <Button transparent>
                      <Icon active name="thumbs-up" />
                    <Text>12 Likes</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button transparent>
                    <Icon active name="chatbubbles" />
                    <Text>4 Comments</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            </Content>
            </Tab>
          <Tab heading="WED">
            <Tab3 />
          </Tab>
          <Tab heading="THU">
            <Tab4 />
          </Tab>
          <Tab heading="FRI">
            <Tab5 />
            <Content>
              <View style={styles.container}>
                <ListView
                  contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={this.state.cordeDataSource}
                  renderRow={this.renderEntryCorde.bind(this)}
                  enableEmptySections={true}
                  style={styles.listvie}
                />
              </View>
            </Content>
          </Tab>
        </Tabs>
        <Footer>
          <FooterTab>
            <Button active>
              <Icon active name="home" />
            </Button>
            <Button>
              <Icon name="bookmarks" onPress= {() => {Actions.PageCloset(); }} />
            </Button>
            <Button>
              <Icon name="cart" onPress= {() => {Actions.PageProposal(); }}/>
            </Button>
            <Button>
              <Icon name="settings" onPress= {() => {Actions.PageSetting(); }}/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
  renderEntryCorde(item){
    return (
      <CordeList item={item}/>
    );
  };
}
