import React, { Component } from 'react';
import { Container, Header, Content, Left, Body, Right, Button, Icon, Title, Footer,
         FooterTab,
 } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ReactNative, { View, Image, StyleSheet, } from 'react-native';
import firebaseApp from '../library/firebaseApp'
import styles from '../styles';

const {
  AppRegistry,
  ListView,
  TouchableHighlight,
  AlertIOS,
} = ReactNative;

const ListItemCorde = require('./ListItemCorde');

var coat = [];
var tops = [];
var bottoms = [];
var cordes = [];


export default class PageProposal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cordeDataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      })
    };
    this.coatItemsRef = this.getRef().child('羽織物');
    this.topsItemsRef = this.getRef().child('トップス');
    this.bottomsItemsRef = this.getRef().child('ボトムス');
  }
  getRef() {
    return firebaseApp.database().ref();
  }

  listenForItems(coatItemsRef, topsItemsRef, bottomsItemsRef) {
    coat.length = 0;
    coat.push('なし');
    tops.length = 0;
    bottoms.length = 0;
    cordes.length = 0;
    coatItemsRef.on('value', (snap) => {
      snap.forEach((child) => {
        coat.push(child.val().fashionId);
      });
    });
    bottomsItemsRef.on('value', (snap) => {
      snap.forEach((child) => {
        bottoms.push(child.val().fashionId);
      });
    topsItemsRef.on('value', (snap) => {
      snap.forEach((child) => {
        tops.push(child.val().fashionId);
      });
    });
    for (var i = 0; i < 20; i++) {
      switch(coat[i]){
        case 'なし':
          for (var j = 0; j < 20; j++) {
          switch (bottoms[j]) {
            case '９分パンツ : OLIVEDRAB':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000001080121');
                    break;
                  case 'カットソー : WHITE':
                    cordes.push('000001080221');
                    break;
                }
              }
              break;
            case '９分パンツ : NAVY':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : PEACH':
                    cordes.push('000001150101');
                    break;
                  case 'シャツ : BLUE':
                    cordes.push('000001150114');
                    break;
                  case 'カットソー : GREY':
                    cordes.push('000001150222');
                    break;
                }
              }
              break;
            case '９分パンツ : CREAM':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : BLUE':
                    cordes.push('000001170114');
                    break;
                  case 'カットソー : BLACK':
                    cordes.push('000001170223');
                    break;
                }
              }
              break;
            case '９分パンツ : WHITE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
//
                  case 'シャツ : PEACH':
                    cordes.push('000001210101');
                    break;
                  case 'シャツ : WATER':
                    cordes.push('000001210109');
                    break;
                  case 'シャツ : BLUE':
                    cordes.push('000001210114');
                    break;
                  case 'シャツ : BLACK':
                    cordes.push('000001210123');
                    break;
                  case 'カットソー : CAMEL':
                    cordes.push('000001210218');
                    break;
//紫ニットのアイテム登録無し
                  case 'ニット : PURPULE':
                    cordes.push('000001210316');
                    break;
                }
              }
              break;
            case '９分パンツ : GREY':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WATER':
                    cordes.push('000001220109');
                    break;
                  case 'シャツ : OLIVEDRAB':
                    cordes.push('000001220112');
                    break;
//茶のカットソー登録無し
                  case 'カットソー : BROWN':
                    cordes.push('000001220219');
                    break;
                  case 'カットソー : GREY':
                    cordes.push('000001220222');
                    break;
                  case 'カットソー : BLACK':
                    cordes.push('000001220223');
                    break;
                }
              }
              break;
            case '９分パンツ : BLACK':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : PEACH':
                    cordes.push('000001230101');
                    break;
                  case 'シャツ : ORANGE':
                    cordes.push('000001230107');
                    break;
                  case 'シャツ : WHITE':
                    cordes.push('000001230121');
                    break;
                  case 'シャツ : BLACK':
                    cordes.push('000001230123');
                    break;
                  case 'カットソー : BORDEAUX':
                    cordes.push('000001230204');
                    break;
                  case 'カットソー : WHITE':
                    cordes.push('000001230221');
                    break;
                  case 'カットソー : GREY':
                    cordes.push('000001230222');
                    break;
                  case 'ニット : BLUE':
                    cordes.push('000001230314');
                    break;
                  case 'ノースリーブ : BLACK':
                    cordes.push('000001230423');
                    break;
                }
              }
              break;
//
            case 'ペンシルスカート : MUSTARD':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000002060223');
                    break;
                }
              }
              break;
            case 'ペンシルスカート : OLIVEDRAB':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WATER':
                    cordes.push('000002080109');
                    break;
                }
              }
              break;
            case 'ペンシルスカート : WATER':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000002130121');
                    break;
                }
              }
              break;
//
            case 'ペンシルスカート : PURPULE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : CREAM':
                    cordes.push('000002160217');
                    break;
                }
              }
              break;
//
            case 'ペンシルスカート : CREAM':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000002170223');
                    break;
//ベージュのノースリーブ無し
                  case 'ノースリーブ : CREAM':
                    cordes.push('000002170417');
                    break;
                }
              }
              break;
//キャメル ペンシルなし
            case 'ペンシルスカート : CAMEL':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000002180121');
                    break;
                }
              }
              break;
            case 'ペンシルスカート : WHITE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000002210113');
                    break;
                }
              }
              break;
            case 'ペンシルスカート : GREY':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000002220121');
                    break;
//
                  case 'シャツ : BLACK':
                    cordes.push('000002220123');
                    break;
//ノースリーブなし
                  case 'ノースリーブ : WHITE':
                    cordes.push('000002220421');
                    break;
                }
              }
              break;
            case 'ペンシルスカート : BLACK':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : BORDEAUX':
                    cordes.push('000002230104');
                    break;
                  case 'シャツ : ORANGE':
                    cordes.push('000002230107');
                    break;
                  case 'シャツ : WATER':
                    cordes.push('000002230109');
                    break;
                  case 'シャツ : OLIVEDRAB':
                    cordes.push('000002230112');
                    break;
                  case 'シャツ : WHITE':
                    cordes.push('000002230121');
                    break;
//
                  case 'シャツ : BLACK':
                    cordes.push('000002230123');
                    break;
                }
              }
              break;
//
            case '膝丈スカート : NAVY':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : YELLOW':
                    cordes.push('000003150105');
                    break;
                }
              }
              break;
//
            case '膝丈スカート : WHITE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'ニースリーブ : NAVY':
                    cordes.push('000003210415');
                    break;
                }
              }
              break;

            case 'ミディスカート : CREAM':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000004170223');
                    break;
                }
              }
              break;
//
            case 'ミディスカート : CAMEL':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WATER':
                    cordes.push('000004180113');
                    break;
                }
              }
              break;
//
            case 'ミディスカート : BROWN':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : BLACK':
                    cordes.push('000004180123');
                    break;
                }
              }
              break;
//
            case 'ミディスカート : BLACK':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : CREAM':
                    cordes.push('000004230217');
                    break;
                }
              }
              break;

            case 'ガウチョパンツ : ORANGE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : WHITE':
                    cordes.push('000005070121');
                    break;
                }
              }
              break;
            case 'ガウチョパンツ : OLIVEDRAB':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000005120223');
                    break;
                }
              }
              break;
            case 'ガウチョパンツ : CREAM':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000005170223');
                    break;
                }
              }
              break;
//
            case 'ガウチョパンツ : BROWN':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'カットソー : BLACK':
                    cordes.push('000005190223');
                    break;
                }
              }
              break;
            case 'ガウチョパンツ : WHITE':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
//
                  case 'シャツ : NAVY':
                    cordes.push('000005210115');
                    break;
                  case 'カットソー : NAVY':
                    cordes.push('000005210215');
                    break;
                }
              }
              break;
            case 'ガウチョパンツ : BLACK':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'シャツ : BLUE':
                    cordes.push('000005230114');
                    break;
                }
              }
              break;
//
            case 'スラックス : GREY':
              for (var k = 0; k < 20; k++) {
                switch (tops[k]) {
                  case 'ノースリーブ : WHITE':
                    cordes.push('000006220421');
                    break;
                }
              }
              break;
            }
          }
          break;
        case 'ジャケット : NAVY':
          for (var j = 0; j < 20; j++) {
            switch (bottoms[j]) {
              case '９分パンツ : WHITE':
                for (var k = 0; k < 20; k++) {
                  switch (tops[k]) {
//
                    case 'カットソー : WHITE':
                      cordes.push('011502210221');
                      break;
                  }
                }
                break;
              case '９分パンツ : GREY':
                for (var k = 0; k < 20; k++) {
                  switch (tops[k]) {
//
                    case 'シャツ : BROWN':
                      cordes.push('011501220119');
                      break;
                  }
                }
                break;
              case 'ペンシルスカート : WHITE':
                for (var k = 0; k < 20; k++) {
                  switch (tops[k]) {
                    case 'カットソー : WHITE':
                      cordes.push('011502210221');
                      break;
                  }
                }
                break;
              }
            }
            break;
          case 'ジャケット : WHITE':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : CREAM':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : WHITE':
                        cordes.push('012101170121');
                        break;
                      }
                    }
                  break;
                case '９分パンツ : CAMEL':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : BLACK':
                        cordes.push('012101180223');
                        break;
                    }
                  }
                  break;
                case '９分パンツ : WHITE':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : BLACK':
                        cordes.push('012101210223');
                        break;
                    }
                  }
                  break;
              }
            }
            break;
          case 'ジャケット : GREY':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : WHITE':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : BORDEAUX':
                        cordes.push('012201210204');
                        break;
                    }
                  }
                  break;
                case '９分パンツ : BLACK':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : WHITE':
                        cordes.push('012201230121');
                        break;
                      case 'シャツ : BLACK':
                        cordes.push('012201230123');
                        break;
                      case 'カットソー : WHITE':
                        cordes.push('012201230221');
                        break;
                    }
                  }
                  break;
              }
            }
            break;
          case 'ジャケット : BLACK':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : CREAM':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : GREY':
                        cordes.push('012301070222');
                        break;
                    }
                  }
                break;
                case '９分パンツ : WHITE':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : WHITE':
                        cordes.push('012301210221');
                        break;
                    }
                  }
                break;
                case '９分パンツ : BLACK':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : BORDEAUX':
                        cordes.push('012301230104');
                        break;
                    }
                  }
                break;
                case '膝丈スカート : PEACH':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : WHITE':
                        cordes.push('012303010121');
                        break;
                    }
                  }
                break;
                case '膝丈スカート : CAMEL':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : WHITE':
                        cordes.push('012303180221');
                        break;
                    }
                  }
                break;
                case 'ミディスカート : PEACH':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : WHITE':
                        cordes.push('012304010121');
                        break;
                    }
                  }
                break;
              }
            }
          break;
          case 'カーディガン : BLUE':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : GREY':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : BLACK':
                        cordes.push('021401220223');
                        break;
                    }
                  }
                break;
              }
            }
          break;
          case 'カーディガン : NAVY':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : WHITE':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'シャツ : GREY':
                        cordes.push('021501210122');
                      break;
                    }
                  }
                break;
                case '９分パンツ : BLACK':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : WHITE':
                        cordes.push('021501230221');
                      break;
                    }
                  }
                break;
              }
            }
          break;
          case 'カーディガン : BLACK':
            for (var j = 0; j < 20; j++) {
              switch (bottoms[j]) {
                case '９分パンツ : GREY':
                  for (var k = 0; k < 20; k++) {
                    switch (tops[k]) {
                      case 'カットソー : GREY':
                        cordes.push('022301220222');
                      break;
                    }
                  }
                break;
              }
            }
          break;
        }
      }
      this.setState({
        cordeDataSource: this.state.cordeDataSource.cloneWithRows(cordes)
      });
    });
  }
  componentDidMount() {
    this.listenForItems(this.coatItemsRef, this.topsItemsRef, this.bottomsItemsRef);
  }
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>おすすめアイテム</Title>
          </Body>
          </Header>
          <Content>
            <View style={styles.container}>
            <ListView
              contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
              dataSource={this.state.cordeDataSource}
              renderRow={this.renderEntry.bind(this)}
              enableEmptySections={true}
              style={styles.listvie}
            />
            </View>
          </Content>
          <Footer>
          <FooterTab>
            <Button>
              <Icon  name="home" onPress= {() => {Actions.PageHome(); }}/>
            </Button>
            <Button>
              <Icon name="bookmarks" onPress= {() => {Actions.PageCloset(); }}/>
            </Button>
            <Button active>
              <Icon name="cart"/>
            </Button>
            <Button>
              <Icon name="settings"  onPress= {() => {Actions.PageSetting(); }}/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
  renderEntry(corde){
    return (
      <ListItemCorde corde={corde}/>
    );
  };
}
