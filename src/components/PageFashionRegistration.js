import React, { Component } from 'react';
import { Container, Content,Header, Left, Body, Right, Button, Icon, Title,
         Footer, FooterTab, List, ListItem, Text,
} from 'native-base';
import { Actions } from 'react-native-router-flux';

export default class PageFashionRegistration extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' onPress= {() => {Actions.PageCloset(); }} />
            </Button>
          </Left>
          <Body>
            <Title>登録</Title>
          </Body>
          <Right></Right>
        </Header>
        <Content>
          <List style={styles.whiteBackground}>
          <ListItem itemDivider>
            <Text>羽織物</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceJacket({category: '羽織物', fashion:'ジャケット'});}}>
            <Text> ジャケット</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceCardigan({category: '羽織物', fashion:'カーディガン'});}}>
            <Text>  カーディガン</Text>
          </ListItem>
          <ListItem>
            <Text>  コート（工事中）</Text>
          </ListItem>

          <ListItem itemDivider>
            <Text>トップス</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceShirt({category:'トップス', fashion:'シャツ'});}}>
            <Text>  シャツ</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceCutAndSewn({category:'トップス', fashion:'カットソー'});}}>
            <Text>  カットソー</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceKnit({category:'トップス', fashion:'ニット'});}}>
            <Text>  ニット</Text>
          </ListItem>

          <ListItem itemDivider>
            <Text>ボトムス</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoicePencilskirt({category:'ボトムス', fashion:'ペンシルスカート'});}}>
            <Text>  ペンシルスカート</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceNinepants({category:'ボトムス', fashion:'９分パンツ'});}}>
            <Text>  ９分パンツ</Text>
          </ListItem>
          <ListItem onPress= {() => {Actions.PageColorChoiceMidiSkirt({category:'ボトムス', fashion:'ミディスカート'});}}>
            <Text>  ミディスカート</Text>
          </ListItem>
          <ListItem　onPress= {() => {Actions.PageColorChoiceGaucho({category:'ボトムス', fashion:'ガウチョパンツ'});}}>
            <Text>  ガウチョパンツ</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>  ワンピース（工事中）</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>セットアップ</Text>
          </ListItem>
          <ListItem>
            <Text>  スーツ</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>アウター</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>シューズ</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>バッグ</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>レッグウェア</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>アクセサリー</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>ファッション雑貨</Text>
          </ListItem>
          <ListItem itemDivider>
            <Text>その他</Text>
          </ListItem>
        </List>
        </Content>
        <Footer>
          <FooterTab>
            <Button>
              <Icon active name="home" onPress= {() => {Actions.PageHome(); }}/>
            </Button>
            <Button active>
              <Icon name="bookmarks" onPress= {() => {Actions.PageCloset();}}/>
            </Button>
            <Button>
              <Icon name="cart" onPress= {() => {Actions.PageProposal(); }}/>
            </Button>
            <Button>
              <Icon name="settings" onPress= {() => {Actions.PageSetting(); }}/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = {
  whiteBackground: {
    backgroundColor: '#FFFFFF',
  },
};
