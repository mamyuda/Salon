import React, { Component } from 'react';
import { Container, Content,Header, Left, Body, Right, Button, Icon, Title,
         Footer, FooterTab, List, ListItem, Text,
} from 'native-base';
import { Actions } from 'react-native-router-flux';


export default class PageSetting extends Component {
  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>設定</Title>
          </Body>
          </Header>
        <Content>
        <List style={styles.whiteBackground}>
        <ListItem itemDivider>
          <Text>身長</Text>
        </ListItem>
        <ListItem>
          <Text> １６０ｃｍ〜１６５ｃｍ</Text>
        </ListItem>
        <ListItem itemDivider>
          <Text>服のサイズ</Text>
        </ListItem>
        <ListItem>
          <Text> S</Text>
        </ListItem>
      </List>
        </Content>
        <Footer>
          <FooterTab>
            <Button>
              <Icon name="home" onPress= {() => {Actions.PageHome(); }}/>
            </Button>
            <Button>
              <Icon name="bookmarks" onPress= {() => {Actions.PageCloset(); }}/>
            </Button>
            <Button>
              <Icon name="cart" onPress= {() => {Actions.PageProposal(); }}/>
            </Button>
            <Button active>
              <Icon name="settings"/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}
const styles = {
  whiteBackground: {
    backgroundColor: '#FFFFFF',
  },
};
