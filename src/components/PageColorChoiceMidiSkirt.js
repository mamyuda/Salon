import React, { Component } from 'react';
import { View, Image, StyleSheet, AlertIOS,} from 'react-native';
import { Container, Content, Header, Left, Body, Right, Button, Icon, Title,
         Footer, FooterTab, Text, Toast, connectStyle,
        } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Col, Row, Grid } from 'react-native-easy-grid';

import firebaseApp from '../library/firebaseApp'

var color = '色を選んでください';
var fashionId = '';


class PageColorChoiceMidiSkirt extends Component {
  constructor(props) {
    super(props);
    this.itemsRef = this.getRef().child('items');
  }
  getRef() {
    return firebaseApp.database().ref();
  }

  render() {
    const fashion = this.props.fashion;
    const category = this.props.category;

    return (
      <Container>
        <Header>
          <Left>
            <Button transparent>
              <Icon name='arrow-back' onPress= {() => {Actions.PageFashionRegistration();}}/>
            </Button>
          </Left>
          <Body>
            <Title>カラー選択</Title>
          </Body>
          <Right></Right>
        </Header>
        <Content>
        <Text>カテゴリー：{fashion}</Text>
        <Text>色：{color}</Text>
        <Text></Text>
          <Grid>
            <Row>
            <Col onPress={ () => {this._pressPeach()}}>
            <Image source={require('../image/color/peach.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
            </Row>
            <Text></Text>

            <Row>
              <Col>
                <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
                <Text></Text>
              </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
            </Row>
            <Text></Text>

            <Row>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col onPress={ () => {this._pressOliveDrab()}}>
            <Image source={require('../image/color/oliveDrab.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            </Row>
            <Text></Text>

            <Row>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
            </Row>
            <Text></Text>

            <Row>
            <Col onPress={ () => {this._pressCream()}}>
            <Image source={require('../image/color/cream.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col onPress={ () => {this._pressCamel()}}>
            <Image source={require('../image/color/camel.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
              <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
            </Row>
            <Text></Text>

            <Row>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
            <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
            <Text></Text>
            </Col>
            <Col>
              <Image source={require('../image/color/empty.png')} style={{height:50, width:50, flex:0.25}}/>
              <Text></Text>
              </Col>
            </Row>
            <Text></Text>
          </Grid>
        </Content>
        <Footer>
          <FooterTab>
            <Button>
              <Icon active name="home" onPress= {() => {Actions.HomePage();}}/>
            </Button>
            <Button active>
              <Icon name="bookmarks" onPress= {() => {Actions.ClosetPage();}}/>
            </Button>
            <Button>
              <Icon name="cart" onPress= {() => {Actions.ProposalPage(); }}/>
            </Button>
            <Button>
              <Icon name="settings" onPress= {() => {Actions.SettingPage();}}/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
  _addItem() {
    const fashion = this.props.fashion;
    const category = this.props.category;
      color = this.color;
      fashionId = fashion + ' : ' + color;
      AlertIOS.alert(
        '登録しますか？',
        fashionId,
        [
          {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
          {text: 'Add', onPress: (text) => {{
            this._writeFashion(category, fashion, color, fashionId)}{(() => {Actions.PageCloset();})()}},
          }
        ],
      );

  }

  _writeFashion(category, fashion, color, fashionId) {
    firebaseApp.database().ref(category).child(fashionId).set({fashion,color,fashionId});
  }

  _pressPeach(){
    this.color = "PEACH"
    {(() => {this._addItem();})()}
  };
  _pressPink(){
    this.color = "PINK"
    {(() => {this._addItem();})()}
  };
  _pressRed(){
    this.color = "RED"
    {(() => {this._addItem();})()}
  };
  _pressBordeaux(){
    this.color = "BORDEAUX"
    {(() => {this._addItem();})()}
  };
  _pressYellow(){
    this.color = "YELLOW"
    {(() => {this._addItem();})()}
  };
  _pressMustard(){
    this.color = "MUSTARD"
    {(() => {this._addItem();})()}
  };
  _pressOrange(){
    this.color = "ORANGE"
    {(() => {this._addItem();})()}
  };
  _pressWater(){
    this.color = "WATER"
    {(() => {this._addItem();})()}
  };
  _pressTurquoise(){
    this.color = "TURQUOISE"
    {(() => {this._addItem();})()}
  };
  _pressGreen(){
    this.color = "GREEN"
    {(() => {this._addItem();})()}
  };
  _pressOliveDrab(){
    this.color = "OLIVEDRAB"
    {(() => {this._addItem();})()}
  };
  _pressLightBlue(){
    this.color = "LIGHT BLUE"
    {(() => {this._addItem();})()}
  };
  _pressBlue(){
    this.color = "BLUE"
    {(() => {this._addItem();})()}
  };
  _pressNavy(){
    this.color = "NAVY"
    {(() => {this._addItem();})()}
  };
  _pressPurpule(){
    this.color = "PURPULE"
    {(() => {this._addItem();})()}
  };
  _pressCream(){
    this.color = "CREAM"
    {(() => {this._addItem();})()}
  };
  _pressCamel(){
    this.color = "CAMEL"
    {(() => {this._addItem();})()}
  };
  _pressBrown(){
    this.color = "Brown"
    {(() => {this._addItem();})()}
  };
  _pressWhite(){
    this.color = "WHITE"
    {(() => {this._addItem();})()}
  };
  _pressGrey(){
    this.color = "GREY"
    {(() => {this._addItem();})()}
  };
  _pressBlack(){
    this.color = "BLACK"
    {(() => {this._addItem();})()}
  };

  _colorInitialize(){
    this.color = ""
  };
}
export default PageColorChoiceMidiSkirt;
