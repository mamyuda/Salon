import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js')
const { View, TouchableHighlight, Text, Image } = ReactNative;

class ListItemBottoms extends Component {
  render() {
    const item = this.props.item;

      var closetName = '';
      var closetImg = {};
      {(() => {
        switch (item.title) {

          case '９分パンツ : PEACH':
            closetName = '９分パンツ（桃）';
            closetImg = require('../image/item/bottom/nine/01nine.png');
            break;

          case '９分パンツ : OLIVEDRAB':
            closetName = '９分パンツ（深緑）';
            closetImg = require('../image/item/bottom/nine/12nine.png');
            break;

            case '９分パンツ : NAVY':
              closetName = '９分パンツ（紺）';
              closetImg = require('../image/item/bottom/nine/15nine.png');
              break;


          case '９分パンツ : CREAM':
                closetName = '９分パンツ（浅黄）';
                closetImg = require('../image/item/bottom/nine/17nine.png');
                break;


          case '９分パンツ : WHITE':
            closetName = '９分パンツ（白）';
            closetImg = require('../image/item/bottom/nine/21nine.png');
            break;

          case '９分パンツ : GREY':
              closetName = '９分パンツ（灰）';
              closetImg = require('../image/item/bottom/nine/22nine.png');
              break;

        case '９分パンツ : BLACK':
            closetName = '９分パンツ（黒）';
            closetImg = require('../image/item/bottom/nine/23nine.png');
            break;

        case 'ペンシルスカート : PEACH':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（桃）';
          closetImg = require('../image/item/bottom/pencil/01pencil.png');
        break;
        case 'ペンシルスカート : PINK':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（ピンク）';
          closetImg = require('../image/item/bottom/pencil/02pencil.png');
        break;
        case 'ペンシルスカート : ORANGE':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（ｵﾚﾝｼﾞ）';
          closetImg = require('../image/item/bottom/pencil/07pencil.png');
        break;
        case 'ペンシルスカート : WATER':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（水）';
          closetImg = require('../image/item/bottom/pencil/13pencil.png');
        break;
        case 'ペンシルスカート : OLIVEDRAB':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（深緑）';
          closetImg = require('../image/item/bottom/pencil/12pencil.png');
        break;

        case 'ペンシルスカート : PURPULE':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（紫）';
          closetImg = require('../image/item/bottom/pencil/16pencil.png');
        break;
        case 'ペンシルスカート : WHITE':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（白）';
          closetImg = require('../image/item/bottom/pencil/21pencil.png');
        break;
        case 'ペンシルスカート : GREY':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（灰）';
          closetImg = require('../image/item/bottom/pencil/22pencil.png');
        break;
        case 'ペンシルスカート : BLACK':
          closetName = 'ﾍﾟﾝｼﾙｽｶｰﾄ（黒）';
          closetImg = require('../image/item/bottom/pencil/23pencil.png');
        break;

        case 'ミディスカート : PEACH':
          closetName = 'ミディスカート（桃）';
          closetImg = require('../image/item/bottom/midiSkirt/01midiSkirt.png');
        break;
        case 'ミディスカート : OLIVEDRAB':
          closetName = 'ミディスカート（深緑）';
          closetImg = require('../image/item/bottom/midiSkirt/12midiSkirt.png');
        break;
        case 'ミディスカート : CREAM':
          closetName = 'ミディスカート（ﾍﾞｰｼﾞｭ）';
          closetImg = require('../image/item/bottom/midiSkirt/17midiSkirt.png');
        break;
        case 'ミディスカート : CAMEL':
          closetName = 'ミディスカート（ｷｬﾒﾙ）';
          closetImg = require('../image/item/bottom/midiSkirt/18midiSkirt.png');
        break;

        case 'ガウチョパンツ : ORANGE':
          closetName = 'ガウチョパンツ（ｵﾚﾝｼﾞ）';
          closetImg = require('../image/item/bottom/gaucho/07gaucho.png');
        break;
        case 'ガウチョパンツ : OLIVEDRAB':
          closetName = 'ガウチョパンツ（深緑）';
          closetImg = require('../image/item/bottom/gaucho/12gaucho.png');
        break;
        case 'ガウチョパンツ : CREAM':
          closetName = 'ガウチョパンツ（ﾍﾞｰｼﾞｭ）';
          closetImg = require('../image/item/bottom/gaucho/17gaucho.png');
        break;
        case 'ガウチョパンツ : WHITE':
          closetName = 'ガウチョパンツ（白）';
          closetImg = require('../image/item/bottom/gaucho/21gaucho.png');
        break;
        case 'ガウチョパンツ : GREY':
          closetName = 'ガウチョパンツ（灰）';
          closetImg = require('../image/item/bottom/gaucho/22gaucho.png');
        break;
        case 'ガウチョパンツ : BLACK':
          closetName = 'ガウチョパンツ（黒）';
          closetImg = require('../image/item/bottom/gaucho/23gaucho.png');
        break;


          default:
            closetName = item.title;
            closetImg = require('../image/koujiNow.png');
            break;
        }
      })()}
      return (
        <TouchableHighlight onPress={this.props.onPress}>
        <View style={{ backgroundColor: 'white', margin: 5, width: 115, height: 130 }}>
        <Image source={closetImg} style={{height:115, width:115}}/>
        <Text>{closetName}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

module.exports = ListItemBottoms;
