import React, { Component } from 'react';
import ReactNative, { View, Image, StyleSheet, } from 'react-native';
import { Container, Content, Header, Left, Body, Right, Title,Footer, FooterTab,
         Button, Icon, Card, CardItem,Tab, Tabs,
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Col, Row, Grid } from 'react-native-easy-grid';
import styles from '../styles';
const ListItemCoat = require('./ListItemCoat');
const ListItemTops = require('./ListItemTops');
const ListItemBottoms = require('./ListItemBottoms');

import firebaseApp from '../library/firebaseApp'

const {
  AppRegistry,
  ListView,
  Text,
  TouchableHighlight,
  AlertIOS,
} = ReactNative;

const Tab1 = () => (<View />);
const Tab2 = () => (<View />);
const Tab3 = () => (<View />);
const Tab4 = () => (<View />);
const Tab5 = () => (<View />);

export default　class PageCloset extends Component {

  constructor(props) {
    super(props);
    this.state = {
      coatDataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      topsDataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
      bottomsDataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),

    };
    this.coatItemsRef = this.getRef().child('羽織物');
    this.topsItemsRef = this.getRef().child('トップス');
    this.bottomsItemsRef = this.getRef().child('ボトムス');
  }
  getRef() {
    return firebaseApp.database().ref();
  }

  listenForCoatItems(coatItemsRef) {
    coatItemsRef.on('value', (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
      items.push({
          title: child.val().fashionId ,
          _key: child.key
        });
      });
      this.setState({
        coatDataSource: this.state.coatDataSource.cloneWithRows(items)
      });
    });
  }

  listenForTopsItems(topsItemsRef) {
    topsItemsRef.on('value', (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
      items.push({
          title: child.val().fashionId ,
          _key: child.key
        });
      });
      this.setState({
        topsDataSource: this.state.topsDataSource.cloneWithRows(items)
      });
    });
  }

  listenForBottomsItems(bottomsItemsRef) {
    bottomsItemsRef.on('value', (snap) => {
      // get children as an array
      var items = [];
      snap.forEach((child) => {
      items.push({
          title: child.val().fashionId ,
          _key: child.key
        });
      });
      this.setState({
        bottomsDataSource: this.state.bottomsDataSource.cloneWithRows(items)
      });
    });
  }


  componentDidMount() {
    this.listenForCoatItems(this.coatItemsRef);
    this.listenForTopsItems(this.topsItemsRef);
    this.listenForBottomsItems(this.bottomsItemsRef);
  }

  render(){
    const data = this.props.data;
    return (
      <Container>
        <Header style={styles.headerBackStyle}>
          <Left>
          </Left>
          <Body>
            <Title>クローゼット</Title>
          </Body>
          <Right>
            <Button transparent onPress= {() => {Actions.PageFashionRegistration(); }}>
              <Icon name="add" />
            </Button>
          </Right>
        </Header>
        <Tabs>
          <Tab heading="羽織物">
            <Tab1/>
              <Content>
                <View style={styles.container}>
                  <ListView
                    contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                    dataSource={this.state.coatDataSource}
                    renderRow={this.renderEntryCoat.bind(this)}
                    enableEmptySections={true}
                    style={styles.listvie}
                  />
                </View>
              </Content>
            </Tab>
          <Tab heading="トップス">
            <Tab2/>
              <Content>
                <View style={styles.container}>
                  <ListView
                    contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                    dataSource={this.state.topsDataSource}
                    renderRow={this.renderEntryTops.bind(this)}
                    enableEmptySections={true}
                    style={styles.listvie}
                  />
                </View>
              </Content>
             </Tab>
            <Tab heading="ボトムス">
             <Tab3/>
              <Content>
                <View style={styles.container}>
                <ListView
                  contentContainerStyle={{ flexDirection: 'row', flexWrap: 'wrap' }}
                  dataSource={this.state.bottomsDataSource}
                  renderRow={this.renderEntryBottoms.bind(this)}
                  enableEmptySections={true}
                  style={styles.listvie}
                />
              </View>
            </Content>
          </Tab>
        </Tabs>
        <Footer>
          <FooterTab>
            <Button>
              <Icon active name="home" onPress= {() => {Actions.PageHome(); }}/>
            </Button>
            <Button active>
              <Icon name="bookmarks" />
            </Button>
            <Button>
              <Icon name="cart" onPress= {() => {Actions.PageProposal(); }}/>
            </Button>
            <Button>
              <Icon name="settings"  onPress= {() => {Actions.PageSetting(); }}/>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
  renderEntryCoat(item){
    const onPress = () => {
      AlertIOS.alert(
        '削除しますか？',
        null,
        [
          {text: 'NO', onPress: (text) => console.log('Cancelled')},
          {text: 'YES', onPress: (text) => this.coatItemsRef.child(item._key).remove()},
        ]
      );
    }
    return (
      <ListItemCoat item={item} onPress={onPress} />
    );
  };
  renderEntryTops(item){
    const onPress = () => {
      AlertIOS.alert(
        '削除しますか？',
        null,
        [
          {text: 'NO', onPress: (text) => console.log('Cancelled')},
          {text: 'YES', onPress: (text) => this.topsItemsRef.child(item._key).remove()},
        ]
      );
    }
    return (
      <ListItemTops item={item} onPress={onPress} />
    );
  };
  renderEntryBottoms(item){
      const onPress = () => {
        AlertIOS.alert(
          '削除しますか？',
          null,
          [
            {text: 'NO', onPress: (text) => console.log('Cancelled')},
            {text: 'YES', onPress: (text) => this.bottomsItemsRef.child(item._key).remove()},
          ]
        );
      }
      return (
        <ListItemBottoms item={item} onPress={onPress} />
      );
    };
}
