import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js')
const { View, TouchableHighlight, Text, Image } = ReactNative;

class ListItemCoat extends Component {
  render() {
    const item = this.props.item;

      var closetName = '';
      var closetImg = {};
      {(() => {
        switch (item.title) {
          case 'ジャケット : PEACH':
            closetName = 'ジャケット（桃）';
            closetImg = require('../image/item/coat/jacket/01jacket.png');
          break;

          case 'ジャケット : RED':
            closetName = 'ジャケット（赤）';
            closetImg = require('../image/item/coat/jacket/03jacket.png');
          break;

          case 'ジャケット : WATER':
            closetName = 'ジャケット（水）';
            closetImg = require('../image/item/coat/jacket/13jacket.png');
          break;

          case 'ジャケット : BLUE':
            closetName = 'ジャケット（青）';
            closetImg = require('../image/item/coat/jacket/14jacket.png');
          break;

          case 'ジャケット : NAVY':
            closetName = 'ジャケット（紺）';
            closetImg = require('../image/item/coat/jacket/15jacket.png');
          break;

          case 'ジャケット : WHITE':
            closetName = 'ジャケット（白）';
            closetImg = require('../image/item/coat/jacket/21jacket.png');
          break;

          case 'ジャケット : GREY':
            closetName = 'ジャケット（灰）';
            closetImg = require('../image/item/coat/jacket/22jacket.png');
          break;

          case 'ジャケット : BLACK':
            closetName = 'ジャケット（黒）';
            closetImg = require('../image/item/coat/jacket/23jacket.png');
          break;


          case 'カーディガン : BLUE':
            closetName = 'カーディガン（青）';
            closetImg = require('../image/item/coat/cardigan/14cardigan.png');
          break;

          case 'カーディガン : NAVY':
            closetName = 'カーディガン（紺）';
            closetImg = require('../image/item/coat/cardigan/15cardigan.png');
          break;

          default:
            closetName = item.title;
            closetImg = require('../image/koujiNow.png');
            break;

        }
      })()}
      return (
        <TouchableHighlight onPress={this.props.onPress}>
        <View style={{ backgroundColor: 'white', margin: 5, width: 115, height: 130 }}>
        <Image source={closetImg} style={{height:115, width:115}}/>
        <Text>{closetName}</Text>
        </View>
      </TouchableHighlight>
    );
  }
}

module.exports = ListItemCoat;
