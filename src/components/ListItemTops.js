import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js')
const { View, TouchableHighlight, Text, Image } = ReactNative;

class ListItemTops extends Component {
  render() {
    const item = this.props.item;
      var closetName = '';
      var closetImg = {};
      {(() => {
        switch (item.title) {
          case 'シャツ : PEACH':
            closetName = 'シャツ（桃）';
            closetImg = require('../image/item/top/shirts/01shirts.png');
          break;

          case 'シャツ : BORDEAUX':
            closetName = 'シャツ（紅）';
            closetImg = require('../image/item/top/shirts/04shirts.png');
          break;

          case 'シャツ : YELLOW':
            closetName = 'シャツ（黄）';
            closetImg = require('../image/item/top/shirts/05shirts.png');
          break;

          case 'シャツ : ORANGE':
            closetName = 'シャツ（オレンジ）';
            closetImg = require('../image/item/top/shirts/07shirts.png');
          break;

          case 'シャツ : OLIVEDRAB':
            closetName = 'シャツ（群青）';
            closetImg = require('../image/item/top/shirts/12shirts.png');
          break;

          case 'シャツ : WATER':
            closetName = 'シャツ（水）';
            closetImg = require('../image/item/top/shirts/13shirts.png');
          break;

          case 'シャツ : BLUE':
            closetName = 'シャツ（青）';
            closetImg = require('../image/item/top/shirts/14shirts.png');
          break;

          case 'シャツ : NAVY':
            closetName = 'シャツ（紺）';
            closetImg = require('../image/item/top/shirts/15shirts.png');
          break;

          case 'シャツ : CREAM':
            closetName = 'シャツ（ベージュ）';
            closetImg = require('../image/item/top/shirts/17shirts.png');
          break;
          case 'シャツ : CAMEL':
            closetName = 'シャツ（キャメル）';
            closetImg = require('../image/item/top/shirts/18shirts.png');
          break;

          case 'シャツ : WHITE':
            closetName = 'シャツ（白）';
            closetImg = require('../image/item/top/shirts/21shirts.png');
          break;
          case 'シャツ : BLACK':
            closetName = 'シャツ（黒）';
            closetImg = require('../image/item/top/shirts/23shirts.png');
          break;
          case 'カットソー : BORDEAUX':
            closetName = 'カットソー（紅）';
            closetImg = require('../image/item/top/cutAndSewn/04cutAndSewn.png');
          break;
          case 'カットソー : NAVY':
            closetName = 'カットソー（紺）';
            closetImg = require('../image/item/top/cutAndSewn/15cutAndSewn.png');
          break;
          case 'カットソー : CREAM':
            closetName = 'カットソー（ベージュ）';
            closetImg = require('../image/item/top/cutAndSewn/17cutAndSewn.png');
          break;
          case 'カットソー : CAMEL':
            closetName = 'カットソー（キャメル）';
            closetImg = require('../image/item/top/cutAndSewn/18cutAndSewn.png');
          break;
          case 'カットソー : WHITE':
            closetName = 'カットソー（白）';
            closetImg = require('../image/item/top/cutAndSewn/21cutAndSewn.png');
          break;
          case 'カットソー : GREY':
            closetName = 'カットソー（灰）';
            closetImg = require('../image/item/top/cutAndSewn/22cutAndSewn.png');
          break;
          case 'カットソー : BLACK':
            closetName = 'カットソー（黒）';
            closetImg = require('../image/item/top/cutAndSewn/23cutAndSewn.png');
          break;

          case 'ニット : BLUE':
            closetName = 'ニット（青）';
            closetImg = require('../image/item/top/knit/14knit.png');
          break;


          default:
            closetName = item.title;
            closetImg = require('../image/koujiNow.png');
          break;
          }
      })()}
    return (
        <TouchableHighlight onPress={this.props.onPress}>
        <View style={{ backgroundColor: 'white', margin: 5, width: 115, height: 130 }}>
        <Image source={closetImg} style={{height:115, width:115}}/>
        <Text>{closetName}</Text>
        </View>
        </TouchableHighlight>
    );
  }
}

module.exports = ListItemTops;
