import React, {Component} from 'react';
import ReactNative from 'react-native';
const styles = require('../styles.js')
const { View, TouchableHighlight, Text, Image } = ReactNative;


class ListItemCorde extends Component {
  render() {
    var corde = this.props.corde;
    var cordeImg = {};

    {(() =>
      {
        switch (corde) {
          case '000001080121':
            cordeImg = require('../image/corde/000001080121.png');
            break;
          case '000001080221':
            cordeImg = require('../image/corde/000001080221.png');
            break;
          case '000001150101':
            cordeImg = require('../image/corde/000001150101.png');
            break;
          case '000001150114':
            cordeImg = require('../image/corde/000001150114.png');
            break;
          case '000001150222':
            cordeImg = require('../image/corde/000001150222.png');
            break;
          case '000001170114':
            cordeImg = require('../image/corde/000001170114.png');
            break;
          case '000001170223':
            cordeImg = require('../image/corde/000001170223.png');
            break;
          case '000001210101':
            cordeImg = require('../image/corde/000001210101.png');
            break;
          case '000001210109':
            cordeImg = require('../image/corde/000001210109.png');
            break;
          case '000001210114':
            cordeImg = require('../image/corde/000001210114.png');
            break;
            case '000001210123':
            cordeImg = require('../image/corde/000001210123.png');
            break;
          case '000001210218':
            cordeImg = require('../image/corde/000001210218.png');
            break;
          case '000001210316':
            cordeImg = require('../image/corde/000001210316.png');
            break;
          case '000001220109':
            cordeImg = require('../image/corde/000001220109.png');
            break;
          case '000001220112':
            cordeImg = require('../image/corde/000001220112.png');
            break;
          case '000001220219':
            cordeImg = require('../image/corde/000001220219.png');
            break;
            case '000001220222':
              cordeImg = require('../image/corde/000001220222.png');
              break;
          case '000001220223':
            cordeImg = require('../image/corde/000001220223.png');
            break;
          case '000001230101':
            cordeImg = require('../image/corde/000001230101.png');
            break;
          case '000001230107':
            cordeImg = require('../image/corde/000001230107.png');
            break;
          case '000001230121':
            cordeImg = require('../image/corde/000001230121.png');
            break;
          case '000001230123':
            cordeImg = require('../image/corde/000001230123.png');
            break;
          case '000001230204':
            cordeImg = require('../image/corde/000001230204.png');
            break;
          case '000001230221':
            cordeImg = require('../image/corde/000001230221.png');
            break;
          case '000001230222':
            cordeImg = require('../image/corde/000001230222.png');
            break;
          case '000001230314':
            cordeImg = require('../image/corde/000001230314.png');
            break;
          case '000001230423':
            cordeImg = require('../image/corde/000001230423.png');
            break;
          case '000002060223':
            cordeImg = require('../image/corde/000002060223.png');
            break;
          case '000002080109':
            cordeImg = require('../image/corde/000002080109.png');
            break;
          case '000002130121':
            cordeImg = require('../image/corde/000002130121.png');
            break;
          case '000002160217':
            cordeImg = require('../image/corde/000002160217.png');
            break;
          case '000002170223':
            cordeImg = require('../image/corde/000002170223.png');
            break;
          case '000002170417':
            cordeImg = require('../image/corde/000002170417.png');
            break;
          case '000002180121':
            cordeImg = require('../image/corde/000002180121.png');
            break;
          case '000002210113':
            cordeImg = require('../image/corde/000002210113.png');
            break;
          case '000002220121':
            cordeImg = require('../image/corde/000002220121.png');
            break;
          case '000002220123':
            cordeImg = require('../image/corde/000002220123.png');
            break;
          case '000002220421':
            cordeImg = require('../image/corde/000002220421.png');
            break;

          case '000002230104':
            cordeImg = require('../image/corde/000002230104.png');
            break;
          case '000002230107':
            cordeImg = require('../image/corde/000002230107.png');
            break;
          case '000002230109':
            cordeImg = require('../image/corde/000002230109.png');
            break;
          case '000002230112':
            cordeImg = require('../image/corde/000002230112.png');
            break;
          case '000002230121':
            cordeImg = require('../image/corde/000002230121.png');
            break;
          case '000002230123':
            cordeImg = require('../image/corde/000002230123.png');
            break;
          case '000003150105':
            cordeImg = require('../image/corde/000003150105.png');
            break;
          case '000003210415':
            cordeImg = require('../image/corde/000003210415.png');
            break;
          case '000004170223':
            cordeImg = require('../image/corde/000004170223.png');
            break;
          case '000004180113':
            cordeImg = require('../image/corde/000004180113.png');
            break;
          case '000004180123':
            cordeImg = require('../image/corde/000004180123.png');
            break;
          case '000004230217':
            cordeImg = require('../image/corde/000004230217.png');
            break;
          case '000005070121':
            cordeImg = require('../image/corde/000005070121.png');
            break;
          case '000005120223':
            cordeImg = require('../image/corde/000005120223.png');
            break;
          case '000005170223':
            cordeImg = require('../image/corde/000005170223.png');
            break;
          case '000005190223':
            cordeImg = require('../image/corde/000005190223.png');
            break;
          case '000005210115':
            cordeImg = require('../image/corde/000005210115.png');
            break;
          case '000005210215':
            cordeImg = require('../image/corde/000005210215.png');
            break;
          case '000005230114':
            cordeImg = require('../image/corde/000005230114.png');
            break;
          case '000006220421':
            cordeImg = require('../image/corde/000006220421.png');
            break;
          case '011501210222':
            cordeImg = require('../image/corde/011501210222.png');
            break;
          case '011501220119':
            cordeImg = require('../image/corde/011501220119.png');
            break;
          case '011502210221':
            cordeImg = require('../image/corde/011502210221.png');
            break;
          case '012101170121':
            cordeImg = require('../image/corde/012101170121.png');
            break;
          case '012101180223':
            cordeImg = require('../image/corde/012101180223.png');
            break;
          case '012101210223':
            cordeImg = require('../image/corde/012101210223.png');
            break;
          case '012201210204':
            cordeImg = require('../image/corde/012201210204.png');
            break;
          case '012201230121':
            cordeImg = require('../image/corde/012201230121.png');
            break;
          case '012201230123':
            cordeImg = require('../image/corde/012201230123.png');
            break;
          case '012201230221':
            cordeImg = require('../image/corde/012201230221.png');
            break;
          case '012301070222':
            cordeImg = require('../image/corde/012301070222.png');
            break;
          case '012301210221':
            cordeImg = require('../image/corde/012301210221.png');
            break;
          case '012301230104':
            cordeImg = require('../image/corde/012301230104.png');
            break;
          case '012303010121':
            cordeImg = require('../image/corde/012303010121.png');
            break;
          case '012303180221':
            cordeImg = require('../image/corde/012303180221.png');
            break;
          case '012304010121':
            cordeImg = require('../image/corde/012304010121.png');
            break;
          case '021401220223':
            cordeImg = require('../image/corde/021401220223.png');
            break;
          case '021501210122':
            cordeImg = require('../image/corde/021501210122.png');
            break;
          case '021501230221':
            cordeImg = require('../image/corde/021501230221.png');
            break;
          case '022301220222':
            cordeImg = require('../image/corde/022301220222.png');
            break;
          default:
            cordeImg = null;
            break;
        }
      }
    )()}
    if(cordeImg == null){
      return (
      <View></View>
      );
    }else {
      return (
      <View style={{ backgroundColor: 'white', margin: 5, height: 390, width: 365}}>
      <Image source={cordeImg} style={{height:365, width:365}}/>
      <Text>{corde}</Text>
      </View>
    );
}
  }
}
module.exports = ListItemCorde;
