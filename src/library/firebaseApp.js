import firebase from 'firebase'
import Config from 'react-native-config'

const config = {
  apiKey: "AIzaSyCGWDFTZFWGNIGHTQPY2RvbM1ZuE4JHdM4",
  authDomain: "sideproject-f35f9.firebaseapp.com",
  databaseURL: "https://sideproject-f35f9.firebaseio.com",
  storageBucket: "sideproject-f35f9.appspot.com",
};
firebase.initializeApp(config)

export default firebase
